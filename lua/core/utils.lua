local M = {}
local merge_tb = vim.tbl_deep_extend

M.load_keymaps = function(section, mapping_opts)
    for mode, mode_values in pairs(section) do
        local default_opts = merge_tb("force", { noremap = true, silent = true, mode = mode }, mapping_opts or {})
        for keybind, mapping_info in pairs(mode_values) do
            local opts = merge_tb("force", default_opts, mapping_info.opts or {})

            mapping_info.opts, opts.mode = nil, nil
            opts.desc = mapping_info[2]

            vim.keymap.set(mode, keybind, mapping_info[1], opts)
        end
    end
end

return M
