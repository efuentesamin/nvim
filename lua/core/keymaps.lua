local M = {}

M.general = {
    i = {
        -- go to  beginning and end
        ["<C-b>"] = { "<ESC>^i", "Beginning of line" },
        ["<C-e>"] = { "<End>", "End of line" },

        -- navigate within insert mode
        ["<C-h>"] = { "<Left>", "Move left" },
        ["<C-l>"] = { "<Right>", "Move right" },
        ["<C-j>"] = { "<Down>", "Move down" },
        ["<C-k>"] = { "<Up>", "Move up" },
    },

    n = {
        ["<Esc>"] = { ":noh <CR>", "Clear highlights" },
        
        -- save
        ["<C-s>"] = { "<cmd> w <CR>", "Save file" },

        -- create splits
        ["<leader>sv"] = {"<C-w>v", "create vertical split"},
        ["<leader>ss"] = {"<C-w>s", "create horizontal split"},
        ["<leader>se"] = {"<C-w>=", "make splits same size"},
        ["<leader>sx"] = {":close<CR>", "close current split"},

        -- move splits
        ["<leader>sh"] = {"<C-w>H", "move split left"},
        ["<leader>sj"] = {"<C-w>J", "move split down"},
        ["<leader>sk"] = {"<C-w>K", "move split up"},
        ["<leader>sl"] = {"<C-w>L", "move split right"},
        ["<leader>st"] = {"<C-w>T", "move split to a new tab"},
        ["<leader>sm"] = {":MaximizerToggle<CR>", "Maximize current split"},

        -- tabs
        ["<leader>to"] = {":tabnew<CR>", "open a new tab"},
        ["<leader>tx"] = {":tabclose<CR>", "close the current tab"},
        ["<leader>tn"] = {":tabn<CR>", "next tab"},
        ["<leader>tp"] = {":tabp<CR>", "prev tab"},
    },
}

M.comment = {
  n = {
    ["<leader>/"] = {
      function()
        require("Comment.api").toggle.linewise.current()
      end,
      "Toggle line comment",
    },
  },

  v = {
    ["<leader>/"] = {
      "<ESC><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<CR>",
      "Toggle line comment",
    },
    ["<leader>b"] = {
      "<ESC><cmd>lua require('Comment.api').toggle.blockwise(vim.fn.visualmode())<CR>",
      "Toggle block comment",
    },
  },
}

M.nvimtree = {
  n = {
    -- toggle
    ["<C-n>"] = { "<cmd> NvimTreeToggle <CR>", "Toggle nvimtree" },

    -- focus
    ["<leader>e"] = { "<cmd> NvimTreeFocus <CR>", "Focus nvimtree" },
  },
}

return M
