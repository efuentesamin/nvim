local opt = vim.opt
local g = vim.g

g.mapleader = " "

g.transparency = false

-- Content
opt.shell = "fish"
opt.autoread = true
opt.path:append("**")
opt.wildignore:append("*/node_modules/*")
opt.wildmenu = true
opt.showmatch = true
opt.title = true
opt.laststatus = 2
opt.scrolloff = 10
vim.o.showtabline = 2

-- Syntax
opt.syntax = "on"

-- Encoding
opt.encoding = "utf-8"
opt.fileencodings = "utf-8"

-- Line numbers
opt.number = true
opt.relativenumber = true

-- Tabs & Indentation
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.expandtab = true
opt.autoindent = true
opt.smartindent = true

-- Line Wrapping
opt.wrap = false

-- Search Settings
opt.ignorecase = true
opt.smartcase = true
opt.incsearch = true
opt.hlsearch = true

-- Appearance
opt.termguicolors = true
opt.background = "dark"

-- Backspace
opt.backspace = "start,eol,indent"

-- Clipboard
opt.clipboard:append("unnamedplus")

-- Split Windows
opt.splitright = true
opt.splitbelow = true

opt.iskeyword:append("-")

