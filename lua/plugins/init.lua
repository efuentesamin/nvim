return {
  "nvim-lua/plenary.nvim",
  
  {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
    config = function()
      vim.cmd([[colorscheme tokyonight]])
    end,
  },

  {
    "christoomey/vim-tmux-navigator",
    lazy = false,
  },

  {
    "szw/vim-maximizer",
    lazy = false,
  },

  {
    "numToStr/Comment.nvim",
    keys = { "gcc", "gbc" },
    init = function()
      local mappings = require("core.keymaps").comment
      require("core.utils").load_keymaps(mappings)
    end,
    config = function()
	  require("Comment").setup()
	end,
  },

  {
    "nanozuki/tabby.nvim",
    lazy = false,
    config = function()
      require("tabby.tabline").use_preset("active_wins_at_tail", {})
    end
  },

  {
    "nvim-tree/nvim-web-devicons",
    lazy = false,
  },
}

